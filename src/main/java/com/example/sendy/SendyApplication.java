package com.example.sendy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * @author timotius.pamungkas
 */
@SpringBootApplication
public class SendyApplication {

	public static void main(String[] args) {
		SpringApplication.run(SendyApplication.class, args);
	}

}
