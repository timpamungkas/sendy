package com.example.sendy.api;

import java.time.LocalDateTime;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @author timotius.pamungkas
 */
@RestController
public class SampleApi {

	@GetMapping("/api/now")
	public String now() {
		return LocalDateTime.now().toString();
	}

}
